# E-Commerce Web

Advanced Web Development project. E-Commerce website.

## Getting Started

### Prerequisites
* XAMPP or WAMP
* Web Browser

### Setup Environment
1. Import the sql database file from res/ecom.sql
2. Copy the downloaded source:
  * 'htdocs' for XAMPP
  * 'www' for WAMP

## Contributing
Please [create a new issue](https://bitbucket.org/acgl/e-commerce/issues/new) if you found bugs or you want to request a feature. PRs are welcome.

This project uses [jade/pug](https://github.com/pugjs/pug) for developement. To get started [here](https://pugjs.org/api/getting-started.html). No future plans to host this.

### VCS
This project uses [git branching model](http://nvie.com/posts/a-successful-git-branching-model/). Please check before creating PR.
Please clone or fork the source.

### Built with
* [Bootstrap](https://github.com/twbs/bootstrap) - used for responsive website
* [jquery](https://github.com/jquery/jquery) - javascript library

### Coding Styles
Please follow the [coding styles](http://codeguide.co/) used in this project.

### Versioning
This project uses [SemVer](http://semver.org/) for versioning.

## Authors
* Jorge Alejo
* Russel Cuenco
* Marco Gotico
* Zaerald Lungos

### License
This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.
